<?php

function agregar_css_js(){
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css');

    wp_enqueue_script('popper','https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', array('jquery'),'1.14',true);
    wp_enqueue_script('bootstrap-js','js/bootstrap.min.js',array('popper'),'4.3',true);
    wp_enqueue_script('bootstrap-js','js/bootstrap.min.js',array('popper'),'4.3',true);

}
add_action('wp_enqueue_scripts','agregar_css_js');
add_theme_support( 'post-thumbnails' );

function your_themes_pagination(){
    global $wp_query;
    echo paginate_links();
}
if ( function_exists( 'add_theme_support' ) ) {
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 150, 150, true ); // default Featured Image dimensions (cropped)
function themeable_custom_header_setup() {
    $args = array(
        'default-image'      => get_template_directory_uri() . '/images/background.jpg',
        'default-text-color' => '000',
        'width'              => 1000,
        'height'             => 250,
        'flex-width'         => true,
        'flex-height'        => true,
    );
    add_theme_support( 'custom-header', $args );
}
add_action( 'after_setup_theme', 'themeable_custom_header_setup' );
    $header_info = array(
        'width'         => 980,
        'height'        => 60,
        'default-image' => get_template_directory_uri() . '/images/background.jpg',
    );
    add_theme_support( 'custom-header', $header_info );

    $header_images = array(
        'background' => array(
            'url'           => get_template_directory_uri() . '/images/background.jpg',
            'thumbnail_url' => get_template_directory_uri() . '/images/background.jpg',
            'description'   => 'background',
             ),
    );
    register_default_headers( $header_images );
}

